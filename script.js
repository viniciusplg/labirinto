const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
]
let linha = 0
let coluna = 0
let sPosition

const labDiv = document.querySelector('#labirinto')
const construirLab = (curr) => {


    const mkDiv = document.createElement('div')
    labDiv.appendChild(mkDiv)
    mkDiv.id = linha
    mkDiv.className = 'linhas'
    linha += 1
    for (let i of curr) {
        const mkInnerDiv = document.createElement('div')
        mkInnerDiv.innerHTML = `${i}`
        mkDiv.appendChild(mkInnerDiv)
        mkInnerDiv.className = coluna
        coluna += 1
        if (i == 'W') {
            mkInnerDiv.className += ' fundoTxt'
        } else {
            mkInnerDiv.className += " fundoSpc"
        }
        if (i == 'S') {
            mkInnerDiv.innerHTML = ''
            mkInnerDiv.id = 'posInicial'
            const mkStart = document.createElement('span')
            mkStart.innerHTML = 'S'
            mkStart.id = 'mkStart'
            mkInnerDiv.appendChild(mkStart)
        }
        if (i == 'F') {
            mkInnerDiv.innerHTML = ''
            mkInnerDiv.id = 'posFinal'
            const mkFinish = document.createElement('span')
            mkFinish.id = 'mkFinish'
            mkInnerDiv.appendChild(mkFinish)
        }
    }
    coluna = 0
}

map.forEach(construirLab)
sPosition = document.querySelector('#mkStart')


const acoesTeclado = (event) => {
    console.log(event.key)
    const classCheck = sPosition.parentElement.className.replace(/\D/gi, '')
    const idCheck = sPosition.parentElement.parentElement.id
    const idCheckNumber = Number(idCheck)
    const linhaInferior = document.getElementById(idCheckNumber + 1)
    const linhaSuperior = document.getElementById(idCheckNumber - 1)

    switch (event.key) {
        case 'ArrowDown':
            if (linhaInferior.children[classCheck].textContent != 'W') {
                linhaInferior.children[classCheck].appendChild(sPosition)

            }
            break
        case 'ArrowRight':
            if (sPosition.parentElement.nextSibling.textContent != 'W') {
                sPosition.parentElement.nextSibling.appendChild(sPosition)
            }
            break
        case 'ArrowUp':
            if (linhaSuperior.children[classCheck].textContent != 'W') {
                linhaSuperior.children[classCheck].appendChild(sPosition)
            }
            break
        case 'ArrowLeft':
            if (sPosition.parentElement.previousSibling != null && sPosition.parentElement.previousSibling.textContent != 'W') {
                sPosition.parentElement.previousSibling.appendChild(sPosition)
            }
        default:
            console.log('Digite uma seta direcional')
    }
    if (sPosition.parentElement.childElementCount == 2) {
        const divVictory = document.createElement('div')
        divVictory.id = 'victory'
        divVictory.innerHTML = '<strong>Você ganhou!!</strong>'
        labDiv.appendChild(divVictory)
        document.removeEventListener('keydown', acoesTeclado)

    }
}

document.addEventListener('keydown', acoesTeclado)

const getButton = document.querySelector('#buttonRestart')
const acaoRestart = (event) => {
    getInitialPos = document.querySelector('#posInicial')
    getInitialPos.appendChild(sPosition)
    document.addEventListener('keydown', acoesTeclado)
    if (document.getElementById('victory') != null){
        document.getElementById('victory').remove()
    }
}

getButton.addEventListener('click', acaoRestart)
