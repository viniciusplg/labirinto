const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
]
let linha = 0
let coluna = 0
let sPosition

const labDiv = document.querySelector('#labirinto')
const construirLab = (curr) => {


    const mkDiv = document.createElement('pre')
    labDiv.appendChild(mkDiv)
    mkDiv.id = linha
    linha += 1
    for (let i of curr) {
        const mkInerDiv = document.createElement('div')
        // const mkInerDiv2 = document.createElement('div')
        mkInerDiv.innerHTML = `${i}`
        mkDiv.appendChild(mkInerDiv)
        // mkInerDiv.appendChild(mkInerDiv2)
        mkInerDiv.className = coluna
        // mkInerDiv2.className = coluna
        coluna += 1
        if (i == 'W') {
            mkInerDiv.className += ' fundoTxt'
            // mkInerDiv2.className += ' fundoTxt'
        } else {
            mkInerDiv.className += " fundoSpc"
            // mkInerDiv2.className += " fundoSpc"
        }
        if (i == 'S') {
            mkInerDiv.innerHTML = ''
            const mkStart = document.createElement('span')
            mkStart.innerHTML = 'S'
            mkStart.id = 'mkStart'
            mkInerDiv.appendChild(mkStart)
        }
    }
    coluna = 0
}

map.forEach(construirLab)
sPosition = document.querySelector('#mkStart')


const acoesTeclado = (event) => {
    console.log(event.key)
    const classCheck = sPosition.parentElement.className.replace(/\D/gi, '')
    const idCheck = sPosition.parentElement.parentElement.id
    const idCheckNumber = Number(idCheck)
    const linhaInferior = document.getElementById(idCheckNumber + 1)
    const linhaSuperior = document.getElementById(idCheckNumber - 1)

    switch (event.key) {
        case 'ArrowDown':
            if (linhaInferior.children[classCheck].textContent != 'W') {
                linhaInferior.children[classCheck].appendChild(sPosition)

            }
            break
        case 'ArrowRight':
            if (sPosition.parentElement.nextSibling.innerText != 'W') {
                sPosition.parentElement.nextSibling.appendChild(sPosition)
            }
            break
        case 'ArrowUp':
            if (linhaSuperior.children[classCheck].textContent == ' ') {
                linhaSuperior.children[classCheck].appendChild(sPosition)
            }
            break
        case 'ArrowLeft':
            if (sPosition.parentElement.previousSibling.innerText == ' ') {
                sPosition.parentElement.previousSibling.appendChild(sPosition)
            }
        default:
            console.log('Digite uma seta direcional')
    }
}

document.addEventListener('keydown', acoesTeclado)
